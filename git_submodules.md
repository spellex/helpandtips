# Using submodules in Git

## Working with repositories that contain submodules

### Cloning a repository that contains submodules

*If you want to clone a repository including its submodules you can use the __--recursive__ parameter*

```sh
git clone --recursive [URL to Git repo]
```

*If you already have cloned a repository and now want to load it’s submodules you have to use __submodule update__*

```sh
git submodule update --init
# if there are nested submodules:
git submodule update --init --recursive
```

### Downloading multiple submodules at once

*Since a repository can include many submodules, downloading them all sequentially can take much time. For this reason __clone__ and __submodule update__ command support the __--jobs__ parameter to fetch multiple submodules at the same time.*

```sh
# download up to 8 submodules at once
git submodule update --init --recursive --jobs 8
git clone --recursive --jobs 8 [URL to Git repo]
# short version
git submodule update --init --recursive -j 8
```

### Pulling with submodules

*Once you have set up the submodules you can update the repository with fetch/pull like you would normally do. To pull everything including the submodules, use the __--recurse-submodules__ and the __--remote parameter__ in the git pull command*

```sh
# pull all changes in the repo including changes in the submodules
git pull --recurse-submodules

# pull all changes for the submodules
git submodule update --remote
```

### Executing a command on every submodule

*Git provides a command that lets us execute an arbitrary shell command on every submodule. To allow execution in nested subprojects the __--recursive__ parameter is supported. For our example we assume that we want to reset all submodules.*

```sh
git submodule foreach 'git reset --hard'
# including nested submodules
git submodule foreach --recursive 'git reset --hard'
```

## Creating repositories with submodules

### Adding a submodule to a Git repository and tracking a branch

*If you add a submodule, you can specify which branch should be tracked via the __-b__ parameter of the __submodule add__ command. The __git submodule init__ command creates the local configuration file for the submodules, if this configuration does not exist.*

```sh
# add submodule and define the master branch as the one you want to track
1) git submodule add -b master [URL to Git repo]
2) git submodule init
```
1. Adds a new submodule to an existing Git repository and defines that the master branch should be tracked

2. Initialize submodule configuration

*If you track branches in your submodules, you can update them via the __--remote__ parameter of the __git submodule update__ command. This pulls in new commits into the main repository and its submodules. It also changes the working directories of the submodules to the commit of the tracked branch.*

```sh
# update your submodule --remote fetches new commits in the submodules
# and updates the working tree to the commit described by the branch
git submodule update --remote
```

## Adding a submodule and tracking commits

*Alternatively to the tracking of a branch, you can also control which commit of the submodule should be used. In this case the Git parent repository tracks the commit that should be checked out in each configured submodule. Performing a submodule update checks out that specific revision in the submodule’s Git repository. You commonly perform this task after you pull a change in the parent repository that updates the revision checked out in the submodule. You would then fetch the latest changes in the submodule’s Git repository and perform a submodule update to check out the current revision referenced in the parent repository. Performing a submodule update is also useful when you want to restore your submodule’s repository to the current commit tracked by the parent repository. This is common when you are experimenting with different checked out branches or tags in the submodule and you want to restore it back to the commit tracked by the parent repository. You can also change the commit that is checked out in each submodule by performing a checkout in the submodule repository and then committing the change in the parent repository.*

*You add a submodule to a Git repository via the __git submodule add__ command.*

```sh
1) git submodule add [URL to Git repo]
2) git submodule init
```
1. Adds a submodule to the existing Git repository
2. Initialize submodule configuration

## Updating which commit your are tracking

*The relevant state for the submodules are defined by the main repository. If you commit in your main repository, the state of the submodule is also defined by this commit.*

*The __git submodule update__ command sets the Git repository of the submodule to that particular commit. The submodule repository tracks its own content which is nested into the main repository. The main repository refers to a commit of the nested submodule repository.*

*Use the __git submodule update__ command to set the submodules to the commit specified by the main repository. This means that if you pull in new changes into the submodules, you need to create a new commit in your main repository in order to track the updates of the nested submodules.*

*The following example shows how to update a submodule to its latest commit in its master branch.*

```sh
# update submodule in the master branch
# skip this if you use --recurse-submodules
# and have the master branch checked out
cd [submodule directory]
git checkout master
git pull

# commit the change in main repo
# to use the latest commit in master of the submodule
cd ..
git add [submodule directory]
git commit -m "move submodule to latest commit in master"

# share your changes
git push
```
